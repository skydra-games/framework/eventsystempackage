using System;
using UnityEngine;
using UnityEngine.Events;

namespace Skydra.EventSystem
{
    public class EventListener : MonoBehaviour
    {
        public EventID targetEvent;
        public UnityEvent<EventArgs> onRaised;

        private void OnEnable()
        {
            EventManager.Register(targetEvent, onRaised.Invoke, true);
        }

        private void OnDisable()
        {
            EventManager.Unregister(targetEvent, onRaised.Invoke);
        }
    }
}