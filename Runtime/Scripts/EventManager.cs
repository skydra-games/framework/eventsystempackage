using System;
using System.Collections.Generic;
using UnityEngine;

namespace Skydra.EventSystem
{
    public static class EventManager
    {
        private static readonly Dictionary<EventID, Event> Events = new Dictionary<EventID, Event>();
        private static bool isInitialized = false;
        
        [RuntimeInitializeOnLoadMethod]
        private static void Initialize()
        {
            foreach (EventID id in Enum.GetValues(typeof(EventID)))
            {
                Events.Add(id, new Event());
            }
                
            Events[EventID.UnregisterAutoEvents].Register(AutoUnregistering);
            isInitialized = true;
        }

        public static void Register(EventID eventID, Action<EventArgs> action, bool autoUnregister = false)
        {
            if (isInitialized)
                Events[eventID].Register(action, autoUnregister);
        }
        
        public static void Unregister(EventID eventID, Action<EventArgs> action)
        {
            if (isInitialized)
                Events[eventID].Unregister(action);
        }

        public static void Raise(EventID eventID, EventArgs args = null)
        {
            if (isInitialized)
                Events[eventID].Raise(args);
        }

        private static void AutoUnregistering(EventArgs args)
        {
            foreach (Event e in Events.Values)
            {
                e.UnregisterAutoEvents();
            }
        }
    }
}