#if UNITY_EDITOR
using System.Collections.Generic;/*
using Skydra.Utility;
using Sirenix.OdinInspector;*/
using UnityEditor;
using UnityEngine;

namespace Skydra.EventSystem
{
    [CreateAssetMenu(fileName = "Events Data", menuName = "Skydra/Event System/Events Data")]

    public class EventsData : ScriptableObject
    {
        public List<string> events = new List<string>();

        //[Button]
        private void RaiseEvent(EventID eventID)
        {
            if (EditorApplication.isPlaying && !EditorApplication.isPaused)
                EventManager.Raise(eventID);
        }

        //[Button]
        private void GenerateEvents()
        {
           /* EnumGenerator.Generate("EventID", "MLFramework.Modules.Systems.EventSystem", "Assets/Modules/Systems/EventSystem/Scripts/",
                events.ToArray());*/
        }
    }
}
#endif
