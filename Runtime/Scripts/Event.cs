using System;

namespace Skydra.EventSystem
{
    public class Event
    {
        public event Action<EventArgs> OnEvent;
        public event Action<EventArgs> OnEventAutoUnregister;

        public void Raise(EventArgs args = null)
        {
            OnEvent?.Invoke(args);
        }

        public void Register(Action<EventArgs> action, bool autoUnregister = false)
        {
            OnEvent += action;
            if (autoUnregister)
                OnEventAutoUnregister += action;
        }

        public void Unregister(Action<EventArgs> action = null)
        {
            OnEvent -= action;
        }

        public virtual void UnregisterAll()
        {
            OnEvent = null;
            OnEventAutoUnregister = null;
        }

        public virtual void UnregisterAutoEvents()
        {
            OnEvent -= OnEventAutoUnregister;
            OnEventAutoUnregister = null;
        }
    }

}