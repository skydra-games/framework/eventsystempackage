namespace Skydra.EventSystem {
	public enum EventID
	{
		UnregisterAutoEvents,
		OnLoadingRequested,
		OnLoadingStart,
		OnLoadingEnd,
		OnLevelStart,
		OnLevelSuccess,
		OnLevelFail,
		OnPause,
		OnResume,
		OnVibrationChange,
		OnEarnCurrency,
		OnSpentCurrency,
		OnItemStored,
		OnSlotCleared,
		MatchItems,
		OnItemsFinished,
		OnTimerEnd,
		OnPointerDown,
		OnPointerDrag,
		OnPointerUp,
		OnFinishPanelShow,
		OnSlotsFulled,
		OnMatchCompleted,
	}
}
