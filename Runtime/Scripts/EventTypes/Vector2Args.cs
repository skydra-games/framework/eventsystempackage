using System;
using UnityEngine;

namespace Skydra.Modules.Systems.EventSystem
{
    public class Vector2Args : EventArgs
    {
        public Vector2 value;

        public Vector2Args(Vector2 value)
        {
            this.value = value;
        }
    }
}
