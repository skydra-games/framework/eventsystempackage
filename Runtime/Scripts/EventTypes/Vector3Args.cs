using System;
using UnityEngine;

namespace Skydra.Modules.Systems.EventSystem
{
    public class Vector3Args : EventArgs
    {
        public Vector3 value;

        public Vector3Args(Vector3 value)
        {
            this.value = value;
        }
    }
}
