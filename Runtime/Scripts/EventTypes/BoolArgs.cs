using System;

namespace Skydra.Modules.Systems.EventSystem
{
    public class BoolArgs : EventArgs
    {
        public bool value;

        public BoolArgs(bool value)
        {
            this.value = value;
        }
    }

}