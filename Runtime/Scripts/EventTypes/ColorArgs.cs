using System;
using UnityEngine;

namespace Skydra.Modules.Systems.EventSystem
{
    public class ColorArgs : EventArgs
    {
        public Color value;

        public ColorArgs(Color value)
        {
            this.value = value;
        }
    }
}