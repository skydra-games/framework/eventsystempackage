using System;

namespace Skydra.Modules.Systems.EventSystem
{
    public class FloatArgs : EventArgs
    {
        public float value;

        public FloatArgs(float value)
        {
            this.value = value;
        }
    }

}