using System;

namespace Skydra.Modules.Systems.EventSystem
{
    public class StringArgs : EventArgs
    {
        public string value;

        public StringArgs(string value)
        {
            this.value = value;
        }
    }
}
