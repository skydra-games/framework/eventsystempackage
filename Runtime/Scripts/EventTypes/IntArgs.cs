using System;

namespace Skydra.Modules.Systems.EventSystem
{
    public class IntArgs : EventArgs
    {
        public int value;

        public IntArgs(int value)
        {
            this.value = value;
        }
    }
}